#version 130

out vec4 vf_Color;
out vec2 vf_TexCoord;

in vec4 gl_Vertex;
in vec4 gl_Color;
in vec4 gl_MultiTexCoord0;

// This is a separate shader for 2D rendering to keep the matrix operations simpler

void main()
{
	mat4 mvp = gl_ModelViewProjectionMatrix;
	vec4 v = vec4(gl_Vertex.xyz, 1.0);

	gl_Position = mvp * v;

	// Pass through OpenGL's built-in variables to the fragment shader
	vf_Color = gl_Color;
	vf_TexCoord = gl_MultiTexCoord0.xy;
}
