#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <assert.h>
#include <stdbool.h>


#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include <GLFW/glfw3.h>

#include "types.h"

#include "shader_loader.h"
#include "shader_loader.c"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "texmgr.c"


#define OFFSET_OF(s, m) (uintptr_t)(&((s *)0)->m)
#define PI (3.141592654)

#define min(a, b) ((a) < (b)) ? (a) : (b)
#define max(a, b) ((a) > (b)) ? (a) : (b)
#define clamp(have, mi, ma) min(max((have), (mi)), (ma))

#define ArrayCount(a) (sizeof(a) / sizeof(a)[0])


/*************************************************************
* Hey, turns out you don't need to write your own math library
* for working with OpenGL
* By using the builtin matrix variables in glsl, it's entirely
* possible to still have control over the vertex data,
* lighting equations, and deferred rendering, and yet
* retain the convenience of using OpenGL's matrix operations!

* -- Note from the future: Not quite :)
*
**************************************************************/

typedef struct
{
	float x;
	float y;
} v2f;

typedef struct
{
	union
	{
		struct
		{
			float x;
			float y;
			float z;
		};
		struct
		{
			float r;
			float g;
			float b;
		};
	};

} v3f;
#define V3F(x, y, z)  (v3f){x, y, z}

typedef struct
{
	float x;
	float y;
	float z;
	float w;
} v4f;

typedef struct
{
	v3f pos;
	v4f color;
} gl_vert;

typedef struct
{
	v3f pos;
	v3f front;
	v3f up;

	// In degrees
	float pitch;
	float yaw;

} camera;

typedef struct
{
	float PerspectiveMatrix[16];
	float ViewMatrix[16];
	bool WireframeEnabled;
	bool DitheringEnabled;

	int ViewportWidth;
	int ViewportHeight;
} renderer;

// Global renderer state
renderer R;

camera CAM_Init()
{
	camera Camera = {0};
	Camera.pos = V3F(0, 0, -1);
	Camera.front = V3F(0, 0, -1);
	Camera.up = V3F(0, 1, 0);

	Camera.pitch = 0;
	Camera.yaw = -90;

	// Nice location for the current scene
	// 0, 0.55, 7
	Camera.pos = V3F(0, 0.55, 7);

	return Camera;
}

typedef struct
{
	u32 width;
	u32 height;
	GLuint fbo;
	GLuint texColor;
	GLuint texDepth;
	GLuint texDither;
} gl_framebuffer;

int Running = 1;


// Not a general purpose function, but parts of it can be extracted
// Written specifically for dithering
gl_framebuffer GL_CreateOffscreenBuffer(u32 width, u32 height)
{
	gl_framebuffer Result = {0};

	Result.width = width;
	Result.height = height;

	glGenBuffers(1, &Result.fbo);

	glGenTextures(1, &Result.texColor);
	glGenTextures(1, &Result.texDepth);
	glGenTextures(1, &Result.texDither);

	// Color texture
	// RGBA16 is good enough here.
	// If you really need better precision you can create this with the RGBA32F internal format
	GLuint COLOR_INTERNAL_FORMAT = GL_RGBA16;
	glBindTexture(GL_TEXTURE_2D, Result.texColor);
	glTexImage2D(GL_TEXTURE_2D, 0, COLOR_INTERNAL_FORMAT, Result.width, Result.height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// Depth texture
	glBindTexture(GL_TEXTURE_2D, Result.texDepth);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Result.width, Result.height, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, 0);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindTexture(GL_TEXTURE_2D, 0);

	// Attach the textures to the framebuffer
	glGenFramebuffers(1, &Result.fbo);
	glBindFramebuffer(GL_FRAMEBUFFER, Result.fbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, Result.texColor, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, Result.texDepth, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// Source for this implementation of dithering
	// https://www.anisopteragames.com/how-to-fix-color-banding-with-dithering/

	unsigned char bayer_pattern[64] = {
    0, 32,  8, 40,  2, 34, 10, 42,   /* 8x8 Bayer ordered dithering  */
    48, 16, 56, 24, 50, 18, 58, 26,  /* pattern.  Each input pixel   */
    12, 44,  4, 36, 14, 46,  6, 38,  /* is scaled to the 0..63 range */
    60, 28, 52, 20, 62, 30, 54, 22,  /* before looking in this table */
    3, 35, 11, 43,  1, 33,  9, 41,   /* to determine the action.     */
    51, 19, 59, 27, 49, 17, 57, 25,
    15, 47,  7, 39, 13, 45,  5, 37,
    63, 31, 55, 23, 61, 29, 53, 21 };

	glGenTextures(1, &Result.texDither);
	glBindTexture(GL_TEXTURE_2D, Result.texDither);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, 8, 8, 0, GL_RED, GL_UNSIGNED_BYTE, &bayer_pattern[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return Result;
}

#include <math.h>


#define V3F_PRIMITIVE_OP(op) \
	result.x = lhs.x op rhs.x; \
	result.y = lhs.y op rhs.y; \
	result.z = lhs.z op rhs.z;

v3f v3f_Add(v3f lhs, v3f rhs)
{
	v3f result;
	V3F_PRIMITIVE_OP(+);

	return result;
}

v3f v3f_Sub(v3f lhs, v3f rhs)
{
	v3f result;
	V3F_PRIMITIVE_OP(-);
	return result;
}

v3f v3f_Mul(v3f lhs, v3f rhs)
{
	v3f result;
	V3F_PRIMITIVE_OP(*);
	return result;
}

v3f v3f_Div(v3f lhs, v3f rhs)
{
	v3f result;
	V3F_PRIMITIVE_OP(/);
	return result;
}

#define V3F_PRIMITIVE_OP_SCALAR(op) \
	result.x = lhs.x op rhs; \
	result.y = lhs.y op rhs; \
	result.z = lhs.z op rhs;

v3f v3f_MulByScalar(v3f lhs, float rhs)
{
	v3f result;
	V3F_PRIMITIVE_OP_SCALAR(*);

	return result;
}


float v3f_Len(v3f v)
{
	v3f tmp;
	float result;
	tmp = v3f_Mul(v, v);
	result = sqrt(tmp.x + tmp.y + tmp.z);

	return result;
}

v3f v3f_Norm(v3f v)
{
	float l = v3f_Len(v);
	v3f result;

	result.x = v.x / l;
	result.y = v.y / l;
	result.z = v.z / l;

	return result;
}

float v3f_Dot(v3f lhs, v3f rhs)
{
	v3f tmp = v3f_Mul(lhs, rhs);
	float result = tmp.x + tmp.y + tmp.z;

	return result;
}

v3f v3f_Cross(v3f lhs, v3f rhs)
{
	// xyzzy
	v3f result;

	result.x = (lhs.y * rhs.z) - (lhs.z * rhs.y);
	result.y = (lhs.z * rhs.x) - (lhs.x * rhs.z);
	result.z = (lhs.x * rhs.y) - (lhs.y * rhs.x);

	return result;
}






/**********************************
* OpenGL shader helpers
**********************************/
void GL_SetProgramInt(GLuint program, const char *name, int value)
{
	GLuint loc = glGetUniformLocation(program, name);
	glUniform1i(loc, value);
}

void GL_SetProgramV3(GLuint program, const char *name, v3f value)
{
	GLuint loc = glGetUniformLocation(program, name);
	glUniform3f(loc, value.x, value.y, value.z);
}


typedef struct
{
	v3f pos;
	v3f normal;
	v2f texcoord;
} model_vert;


typedef struct
{
	model_vert *verts;
	unsigned int vertCount;
} mesh;


/**********************************
* GLFW related
**********************************/
void GLFW_ToggleFullscreen(GLFWwindow *_Window)
{
	struct windowplacement
	{
		int x;
		int y;
		int w;
		int h;
	};

	static struct windowplacement ws;
	static int initialized = 0;

	GLFWmonitor *_WindowMonitor = glfwGetWindowMonitor(_Window);
	const GLFWvidmode *_Vidmode = NULL;

	// Currently fullscreen, set to windowed
	if(_WindowMonitor)
	{
		// Init windowplacement struct
		if(!initialized)
		{
			_Vidmode = glfwGetVideoMode(_WindowMonitor);
			ws.w = _Vidmode->width / 2;
			ws.h = _Vidmode->height / 2;

			ws.x = (_Vidmode->width / 2) - (ws.w / 2);
			ws.y = (_Vidmode->height / 2) - (ws.h / 2);
		}

		// Maybe we should set the refresh rate explicitly
		glfwSetWindowMonitor(_Window, 0, ws.x, ws.y, ws.w, ws.h, GLFW_DONT_CARE);
	}
	// Currently windowed, set to fullscreen
	else
	{
		// Cache window placement
		glfwGetWindowPos(_Window, &ws.x, &ws.y);
		glfwGetWindowSize(_Window, &ws.w, &ws.h);

		// Fullscreen dimensions
		GLFWmonitor *_PrimaryMonitor = glfwGetPrimaryMonitor();
		_Vidmode = glfwGetVideoMode(_PrimaryMonitor);
		glfwSetWindowMonitor(_Window, _PrimaryMonitor, 0, 0, _Vidmode->width, _Vidmode->height, GLFW_DONT_CARE);

		// Hack
		// If we begin in windowed mode, this counts as an initialization
		initialized = 1;
	}
}



void CbResize(GLFWwindow *Window, int w, int h)
{
	glViewport(0, 0, w, h);

	R.ViewportWidth = w;
	R.ViewportHeight = h;
}

void CbWindowClose(GLFWwindow *Window)
{
	Running = 0;
}

void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	#define KEY_PRESSED(k) if(k == key && action == GLFW_PRESS)
	KEY_PRESSED(GLFW_KEY_Q)
	{
		Running = 0;
	}

	KEY_PRESSED(GLFW_KEY_F)
	{
		GLFW_ToggleFullscreen(Window);
	}

	KEY_PRESSED(GLFW_KEY_ESCAPE)
	{
		Running = 0;
	}

	KEY_PRESSED(GLFW_KEY_V)
	{
		R.WireframeEnabled = !R.WireframeEnabled;
	}

	KEY_PRESSED(GLFW_KEY_1)
	{
		R.DitheringEnabled = true;
	}

	KEY_PRESSED(GLFW_KEY_2)
	{
		R.DitheringEnabled = false;
	}

}


#include "cube3d.c"
#include "sphere3d.c"

v3f cubePositions[] =
{
        V3F( 0.0f,  0.0f,  0.0f),
        V3F( 2.0f,  5.0f, -15.0f),
        V3F(-1.5f, -2.2f, -2.5f),
        V3F(-3.8f, -2.0f, -12.3f),
        V3F( 2.4f, -0.4f, -3.5f),
        V3F(-1.7f,  3.0f, -7.5f),
        V3F( 1.3f, -2.0f, -2.5f),
        V3F( 1.5f,  2.0f, -2.5f),
        V3F( 1.5f,  0.2f, -1.5f),
        V3F(-1.3f,  1.0f, -1.5f)
};

// Light wrapper for gluLookAt
void GL_LookAt(v3f pos, v3f fwd, v3f up)
{
	// Centre
	v3f c = v3f_Add(pos, fwd);

	gluLookAt(
			pos.x, pos.y, pos.z,
			c.x, c.y, c.z,
			up.x, up.y, up.z);

	// @Speed
	// Store the view matrix for lighting calculations
	// When sending lighting information to the shader, the position vector must be pre-multiplied to be in view space!
	glGetFloatv(GL_MODELVIEW_MATRIX, R.ViewMatrix);
}





// Global game state
typedef struct
{
	camera Camera;
} game;

game Game;


/**********************************
* General math
**********************************/
float Radians(float degrees)
{
	return degrees * (PI / 180.0);
}

v3f LookDir(float pitch, float yaw)
{
	v3f dir;
	yaw = Radians(yaw);
	pitch = Radians(pitch);
	dir.x = cosf(yaw) * cosf(pitch);
	dir.y = sinf(pitch);
	dir.z = sinf(yaw) * cosf(pitch);

	return dir;
}

void ProcessInput(GLFWwindow *Window, camera *Camera, float dtSec)
{
	#define KEY_IMM(k) if(glfwGetKey(Window, k) == GLFW_PRESS)
	float camspeed = 1.5 * dtSec;
	float strafespeed = 2.5 * dtSec;
	bool UpdateDirection = false;
	float mousesens = 6;
	float mousespeed = 15 * mousesens * dtSec;

	// Forward
	KEY_IMM(GLFW_KEY_W)
	{
		Camera->pos = v3f_Add(Camera->pos, v3f_MulByScalar(Camera->front, camspeed));
	}

	// Back
	KEY_IMM(GLFW_KEY_S)
	{
		Camera->pos = v3f_Sub(Camera->pos, v3f_MulByScalar(Camera->front, camspeed));
	}

	// Strafe left
	KEY_IMM(GLFW_KEY_A)
	{
		Camera->pos = v3f_Sub(Camera->pos, v3f_MulByScalar(v3f_Norm(v3f_Cross(Camera->front, Camera->up)), strafespeed));
	}

	// Strafe right
	KEY_IMM(GLFW_KEY_D)
	{
		Camera->pos = v3f_Add(Camera->pos, v3f_MulByScalar(v3f_Norm(v3f_Cross(Camera->front, Camera->up)), strafespeed));
	}

	// Look left
	KEY_IMM(GLFW_KEY_LEFT)
	{
		Camera->yaw -= mousespeed;
		UpdateDirection = true;
	}

	// Look right
	KEY_IMM(GLFW_KEY_RIGHT)
	{
		Camera->yaw += mousespeed;
		UpdateDirection = true;
	}

	// Look up
	KEY_IMM(GLFW_KEY_UP)
	{
		Camera->pitch += mousespeed;
		Camera->pitch = clamp(Camera->pitch, -89, 89);
		UpdateDirection = true;
	}

	// Look down
	KEY_IMM(GLFW_KEY_DOWN)
	{
		Camera->pitch -= mousespeed;
		Camera->pitch = clamp(Camera->pitch, -89, 89);
		UpdateDirection = true;
	}

	// Ascend
	KEY_IMM(GLFW_KEY_SPACE)
	{
		Camera->pos.y += camspeed;
	}

	// Descend
	KEY_IMM(GLFW_KEY_LEFT_SHIFT)
	{
		Camera->pos.y -= camspeed;
	}

	if(UpdateDirection)
	{
		v3f dir = LookDir(Camera->pitch, Camera->yaw);
		Camera->front = v3f_Norm(dir);
	}
}

void GL_BindTextureUnitWith(GLuint UnitIndex, GLuint TextureHandle)
{
	glActiveTexture(GL_TEXTURE0 + UnitIndex);
	glBindTexture(GL_TEXTURE_2D, TextureHandle);
}

void GL_SelectTextureUnit(GLuint UnitIndex)
{
	glActiveTexture(GL_TEXTURE0 + UnitIndex);
}

void GL_ImmediateCube(v3f pos, v3f color, float scale)
{
    static float cube[] =
	{
		-0.5f, -0.5f, -0.5f,
		0.5f, -0.5f, -0.5f,
		0.5f,  0.5f, -0.5f,
		0.5f,  0.5f, -0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f, -0.5f,  0.5f,
		0.5f, -0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,

		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f, -0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,

		0.5f,  0.5f,  0.5f,
		0.5f,  0.5f, -0.5f,
		0.5f, -0.5f, -0.5f,
		0.5f, -0.5f, -0.5f,
		0.5f, -0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,

		-0.5f, -0.5f, -0.5f,
		0.5f, -0.5f, -0.5f,
		0.5f, -0.5f,  0.5f,
		0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f,  0.5f,
		-0.5f, -0.5f, -0.5f,

		-0.5f,  0.5f, -0.5f,
		0.5f,  0.5f, -0.5f,
		0.5f,  0.5f,  0.5f,
		0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f,  0.5f,
		-0.5f,  0.5f, -0.5f,
	};

	glUseProgram(0);
	glDisable(GL_TEXTURE_2D);

	// Any immediate mode rendering now needs the view matrix explicitly loaded
	glPushMatrix();
	glLoadMatrixf(R.ViewMatrix);
	glTranslatef(pos.x, pos.y, pos.z);
	glScalef(scale, scale, scale);

	glBegin(GL_TRIANGLES);
	glColor3f(color.r, color.g, color.b);
	for(int v = 0; v < ArrayCount(cube); v += 3)
	{
		glVertex3f(cube[v + 0], cube[v + 1], cube[v + 2]);
	}
	glEnd();

	glPopMatrix();
}

int main(int argc, char **argv)
{
	R.WireframeEnabled = 0;

	glfwInit();

	GLFWwindow *Window = glfwCreateWindow(800, 600, "gl-shader-demo", 0, 0);
	glfwMakeContextCurrent(Window);
	glewInit();

	glfwSetKeyCallback(Window, CbKeys);
	glfwSetFramebufferSizeCallback(Window, CbResize);
	glfwSetWindowCloseCallback(Window, CbWindowClose);


	image CubeImage = TEX_Load("res/awesomeface.png", true);
	u32 CubeTexture = TEX_AllocateTextureFromImage(&CubeImage, true, TEX_FILTER_NEAREST | TEX_CLAMP);

	image CrateImage = TEX_Load("res/container.jpg", true);
	u32 CrateTexture = TEX_AllocateTextureFromImage(&CrateImage, true, TEX_FILTER_NEAREST | TEX_CLAMP);

	image TileImage = TEX_Load("res/tile3.png", true);
	u32 TileTexture = TEX_AllocateTextureFromImage(&TileImage, true, TEX_FILTER_NEAREST);


	GLuint progLighting = ShaderProgramCreate("tri.vert", "tri.frag");
	GLuint progDither = ShaderProgramCreate("2d.vert", "dither.frag");
	GLuint progNodither = ShaderProgramCreate("2d.vert", "nodither.frag");

	gl_framebuffer DitherFramebuffer = GL_CreateOffscreenBuffer(1024, 1024);

	gl_vert triangle[] =
	{
		{-0.5, -0.5, 0, 	1, 0, 0, 1},
		{+0.0, +0.5, 0, 	0, 1, 0, 1},
		{+0.5, -0.5, 0, 	0, 0, 1, 1},
	};
	(void)triangle;

	GLuint cube;
	glGenBuffers(1, &cube);
	glBindBuffer(GL_ARRAY_BUFFER, cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube3D), cube3D, GL_STATIC_DRAW);

	GLuint sphere;
	glGenBuffers(1, &sphere);
	glBindBuffer(GL_ARRAY_BUFFER, sphere);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere3D), sphere3D, GL_STATIC_DRAW);

	// Initial state of the shader programs
	glUseProgram(progLighting);
	GL_SetProgramInt(progLighting, "TEX1", 0);
	GL_SetProgramInt(progLighting, "TEX2", 1);

	glUseProgram(progDither);
	GL_SetProgramInt(progDither, "TexFrame", 0);
	GL_SetProgramInt(progDither, "TexDither", 1);

	glUseProgram(progNodither);
	GL_SetProgramInt(progDither, "TexFrame", 0);

	{
		float aspect = 16.0 / 10.0;
		float fov = 90;
		float near = 0.01;
		float far = 100;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(fov, aspect, near, far);
		glGetFloatv(GL_PROJECTION_MATRIX, R.PerspectiveMatrix);
	}


	camera Cam = CAM_Init();

	glUseProgram(progLighting);
	GLuint locViewMatrix = glGetUniformLocation(progLighting, "mat_View");

	glfwPollEvents();
	glfwGetFramebufferSize(Window, &R.ViewportWidth, &R.ViewportHeight);

	double dt = 0.016;
	while(Running)
	{
		double t1 = glfwGetTime();
		ProcessInput(Window, &Cam, dt);


		/**********************************
		* Init state
		**********************************/
		// Init any OpenGL state here
		glUseProgram(progLighting);
		// glClearColor(0.2, 0.3, 0.3, 1);
		glClearColor(0, 0, 0, 0);
		glEnable(GL_NORMALIZE);


		glEnable(GL_DEPTH_TEST);

		// Render to offscreen buffer
		// Note: The viewport here must match the framebuffer dimensions
		glBindFramebuffer(GL_FRAMEBUFFER, DitherFramebuffer.fbo);
		glViewport(0, 0, DitherFramebuffer.width, DitherFramebuffer.height);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		glMatrixMode(GL_PROJECTION);
		glLoadMatrixf(R.PerspectiveMatrix);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		// This is our camera. It should be the first transformation that the modelview matrix undergoes
		// This matrix must be saved for all subsequenty model transformations
		// NO LONGER TRUE. We pass the view to the shader separately and multiply it there
		GL_LookAt(Cam.pos, Cam.front, Cam.up);
		glLoadIdentity();

		glUseProgram(progLighting);

		// We pass in the view matrix separately so that we can calculate the lighting in world space
		// This means we incur a penalty in the vertex shader by doing 2 additional matrix multiplications
		// They should be done on the cpu side if speed ever becomes an issue
		glUniformMatrix4fv(locViewMatrix, 1, GL_FALSE, R.ViewMatrix);

		/**********************************
		* Render
		**********************************/
#if 1
		// If we disable GL_COLOR_ARRAY, we can still call glColor3f and have it passed to our shader

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


		glBindBuffer(GL_ARRAY_BUFFER, cube);

		glEnable(GL_TEXTURE_2D);

		// Calling ActiveTexture makes all OpenGL texture calls act on that texture unit
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, CubeTexture);

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, CrateTexture);

		glActiveTexture(GL_TEXTURE0);

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_NORMAL_ARRAY);
		glEnableClientState(GL_TEXTURE_COORD_ARRAY);


		// glVertexPointer(3, GL_FLOAT, TRI_STRIDE, (void *)OFFSET_OF(gl_vert, pos));
		// glColorPointer(4, GL_FLOAT, TRI_STRIDE, (void *)OFFSET_OF(gl_vert, color));
// For the cube
#define OFFSET_VERT (0 * 4)
#define OFFSET_TEXCOORD (6 * 4)
#define OFFSET_NORMALS (3 * 4)
#define VERT_COUNT 36
#define CUBE_STRIDE (3*4 + 3*4 + 2*4) // 3f for pos, 3f for normal 2f for texture coord

		glVertexPointer(3, GL_FLOAT, CUBE_STRIDE, (void *)OFFSET_VERT);
		glNormalPointer(GL_FLOAT, CUBE_STRIDE, (void *)OFFSET_NORMALS);
		glTexCoordPointer(2, GL_FLOAT, CUBE_STRIDE, (void *)OFFSET_TEXCOORD);

		for(int i = 0; i < ArrayCount(cubePositions); i++)
		{
			v3f p = cubePositions[i];

			glPushMatrix();
			glTranslatef(p.x, p.y, p.z);

			float t = glfwGetTime();
			glRotatef(t*50 + (i * 50), 1, 1, 1);

			glColor3f(1, 1, 1);
			glDrawArrays(GL_TRIANGLES, 0, VERT_COUNT);

			if(R.WireframeEnabled)
			{
				// Wireframe test
				// Todo: Put this, and all other draw calls in a command list

				glDisable(GL_DEPTH_TEST);
				glLineWidth(2);
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				glDisable(GL_TEXTURE_2D);
				glColor3f(1, 0, 1);
				glDrawArrays(GL_TRIANGLES, 0, VERT_COUNT);
				glEnable(GL_TEXTURE_2D);
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glEnable(GL_DEPTH_TEST);
			}

			glPopMatrix();
		}

		/**********************************
		* Draw the light sphere
		**********************************/
		glBindBuffer(GL_ARRAY_BUFFER, sphere);

		glDisableClientState(GL_TEXTURE_COORD_ARRAY);

		glVertexPointer(3, GL_FLOAT, CUBE_STRIDE, 0);
		glNormalPointer(GL_FLOAT, CUBE_STRIDE, (void *)OFFSET_NORMALS);
		// glTexCoordPointer(2, GL_FLOAT, CUBE_STRIDE, (void *)OFFSET_TEXCOORD);

		glColor3f(0, 1, 1);

		glPushMatrix();
		glTranslatef(0, 10, 0);
		glScalef(0.5, 0.5, 0.5);
		glDrawArrays(GL_TRIANGLES, 0, ArrayCount(sphere3D));
		glPopMatrix();

		glPushMatrix();
		glTranslatef(0, -2, 0);
		glScalef(0.5, 0.5, 0.5);
		glDrawArrays(GL_TRIANGLES, 0, ArrayCount(sphere3D));
		glPopMatrix();

		glDisableClientState(GL_VERTEX_ARRAY);
		glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		glDisableClientState(GL_NORMAL_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

#endif


		// Create a flat plane
		float plane = 50;
		float planeElevation = -5;


		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, 0);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, TileTexture);


		glPushMatrix();
		// glRotatef(t * 50, 0, 1, 0);
		// glTranslatef(0, planeElevation, 0);
		glBegin(GL_QUADS);
		glColor3f(1, 1, 1);
		float repeat = 20; // 20

		glTexCoord2f(0, 0);
		glNormal3f(0, 1, 0);
		glVertex3f(-plane, planeElevation, -plane);

		glTexCoord2f(0, repeat);
		glNormal3f(0, 1, 0);
		glVertex3f(-plane, planeElevation, plane);

		glTexCoord2f(repeat, repeat);
		glNormal3f(0, 1, 0);
		glVertex3f(plane, planeElevation, plane);

		glTexCoord2f(repeat, 0);
		glNormal3f(0, 1, 0);
		glVertex3f(plane, planeElevation, -plane);

		glEnd();

		glPopMatrix();

		v3f lightpos = {0, 10, 0.0};
		v3f lightcolor = {1, 1, 1};
		GL_ImmediateCube(lightpos, lightcolor, 0.5);

		v3f cpos = {0, -4, 0};
		v3f ccolor = {0.2, 0.1, 0.3};
		GL_ImmediateCube(cpos, ccolor, 0.5);


		/**********************************
		* Display the offscreen buffer
		**********************************/
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glViewport(0, 0, R.ViewportWidth, R.ViewportHeight);
		glEnable(GL_TEXTURE_2D);
		glDisable(GL_DEPTH_TEST);
		glClear(GL_COLOR_BUFFER_BIT);

		if(R.DitheringEnabled)
		{
			glUseProgram(progDither);
			GL_BindTextureUnitWith(1, DitherFramebuffer.texDither);
		}
		else
		{
			glUseProgram(progNodither);
		}

		GL_BindTextureUnitWith(0, DitherFramebuffer.texColor);

		// We don't save the matrix state since this is the last thing to be rendered
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		glColor3f(1, 1, 1);

		glBegin(GL_QUADS);

		glTexCoord2f(0, 0); glVertex2f(-1, -1);
		glTexCoord2f(0, 1); glVertex2f(-1, 1);
		glTexCoord2f(1, 1); glVertex2f(1, 1);
		glTexCoord2f(1, 0); glVertex2f(1, -1);


		glEnd();


		glfwSwapBuffers(Window);

		double t2 = glfwGetTime();
		dt = (t2 - t1);

		glfwPollEvents();
	}
}
