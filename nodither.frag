#version 130

uniform sampler2D TexFrame;

in vec2 vf_TexCoord;
in vec4 vf_Color;

void main()
{
	vec4 texColor = texture(TexFrame, vf_TexCoord);
	vec3 c = texColor.xyz * vf_Color.xyz;
	float a = texColor.a * vf_Color.a;

	gl_FragColor = vec4(c, 1);
}
