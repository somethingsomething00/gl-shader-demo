#version 130

#extension GL_ARB_gpu_shader5 : enable

out vec4 vf_Color;
out vec2 vf_TexCoord;
out vec3 vf_Normal;
out vec3 vf_FragPos;


in vec4 gl_Vertex;
in vec4 gl_Color;
in vec4 gl_MultiTexCoord0;

uniform mat4 mat_View;

void main()
{
	// All matrix stuff
	mat4 mvp = gl_ModelViewProjectionMatrix;
	mat4 mv = gl_ModelViewMatrix;

	vec4 v = vec4(gl_Vertex.xyz, 1.0);

	// Matrix always goes first
	// Note: In this case, mv is just the model matrix
	// mat_View is sent in by the application rather than by OpenGL
	gl_Position = gl_ProjectionMatrix * mat_View * mv * v;

	// Pass through OpenGL's built-in variables to the fragment shader
	vf_Color = gl_Color;
	vf_TexCoord = gl_MultiTexCoord0.xy;
	vf_Normal = gl_NormalMatrix * gl_Normal.xyz; 
	vf_FragPos = vec3(mv * v);


	// Notes:
	// The normal matrix is conveniently included for lighting calculations :)
	// It is accessed with gl_NormalMatrix
	// It is required to calculate lighting on models that have undergone any transformations from their origin
	// In other words, calling glTranslatef, glScalef, glRotatef... and so on
}
