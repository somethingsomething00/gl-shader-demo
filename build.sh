#!/bin/sh
cc="gcc"
src="gl-demo.c"
bin="gl-demo"
libs="-lm -lGL -lGLU -lGLEW -lglfw"
$cc "$src" -o "$bin" $libs $obj -Wall -Wno-missing-braces
