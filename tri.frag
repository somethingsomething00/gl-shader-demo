#version 130

in vec4 vf_Color;
in vec2 vf_TexCoord;
in vec3 vf_FragPos;
in vec3 vf_Normal;

uniform sampler2D TEX1;
uniform sampler2D TEX2;


uniform vec3 LIGHTPOS;

struct light
{
	vec3 pos;
	vec3 color;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	vec3 attenuation;
};

// Lighting
vec3 ambient = vec3(0.1, 0.1, 0.1);

vec3 LIGHT_CalcDiffuse(vec3 lightpos, vec3 lightcolor, vec3 fragpos, vec3 fragnorm)
{
	fragnorm = normalize(fragnorm);
	vec3 lightdir = normalize(lightpos - fragpos);
	float diffuseFactor = max(dot(fragnorm, lightdir), 0);
	vec3 diffuse = diffuseFactor * lightcolor;

	return diffuse;
}

vec3 LIGHT_CalcDiffuse(light Light, vec3 fragpos, vec3 fragnorm)
{
	return LIGHT_CalcDiffuse(Light.pos, Light.diffuse * Light.color, fragpos, fragnorm);
}

float LIGHT_CalcAttenuation(light Light, vec3 fragpos)
{
	float dist = length(Light.pos - fragpos);
	float c = Light.attenuation.x;
	float l = Light.attenuation.y * dist;
	float q = Light.attenuation.z * (dist * dist);

	return 1.0 / (c + l + q);
}



light LIGHTS[16];

vec3 LIGHTS_CalcDiffuse(int numlights, vec3 fragpos, vec3 fragnorm)
{
	vec3 diffuse = vec3(0);
	fragnorm = normalize(fragnorm);

	for(int i = 0; i < numlights; i++)
	{
		light Light = LIGHTS[i];
		vec3 d = LIGHT_CalcDiffuse(Light, fragpos, fragnorm);
		float a = LIGHT_CalcAttenuation(Light, fragpos);
		diffuse += (d * a);
	}

	return diffuse;
}

light LIGHT_Init()
{
	light Light;

	Light.color = vec3(0, 1, 1);
	Light.diffuse = vec3(1, 1, 1);
	Light.ambient = vec3(0.2, 0.1, 0.3);
	Light.pos = vec3(0, 10, 0);
	Light.attenuation.x = 1;
	Light.attenuation.y = 0.45;
	Light.attenuation.z = 0.0075;

	return Light;

}

light LIGHT_InitOuter()
{
	light Light;

	Light.color = vec3(0.2, 0, 0);
	Light.diffuse = vec3(1, 1, 1);
	Light.ambient = vec3(0.2, 0.1, 0.3);
	Light.pos = vec3(0, 10, 0);
	Light.attenuation.x = 1;
	Light.attenuation.y = 0.014;
	Light.attenuation.z = 0.0009;

	return Light;

}

void PrepLights()
{
	LIGHTS[0] = LIGHT_Init();
	LIGHTS[1] = LIGHT_Init();
	LIGHTS[2] = LIGHT_InitOuter();
	LIGHTS[3] = LIGHT_InitOuter();
	LIGHTS[4] = LIGHT_InitOuter();
	LIGHTS[5] = LIGHT_InitOuter();
	LIGHTS[1].pos = vec3(0, -2, 0);

	float plane = -40;
	// Edge lights
	LIGHTS[2].pos = vec3(-plane, -2, -plane);
	LIGHTS[3].pos = vec3(-plane, -2, plane);
	LIGHTS[4].pos = vec3(plane, -2, plane);
	LIGHTS[5].pos = vec3(plane, -2, -plane);

	vec3 c = vec3(0.2, 0.1, 0.1);
	// Colors
	LIGHTS[2].color = c;
	LIGHTS[3].color = c;
	LIGHTS[4].color = c;
	LIGHTS[5].color = c;
}


void main()
{
	PrepLights();

	vec4 texcolor1 = texture(TEX1, vf_TexCoord);
	vec4 texcolor2 = texture(TEX2, vf_TexCoord);
	vec4 texcolor = mix(texcolor1, texcolor2, 0.2);
	// texcolor = texcolor1;

	light Light;

	Light.ambient = vec3(0.2, 0.1, 0.3);
	float ambientFactor = 0.5;
	Light.ambient *= ambientFactor;

	vec3 finalLight = vec3(1);
	Light.color = vec3(0, 1, 1);
	// Light.color = vec3(0.2, 0.1, 0.3);
	Light.diffuse = vec3(1, 1, 1);
	Light.pos = vec3(0, 10, 0);

	vec3 diffuse = vec3(1);

	if(gl_FrontFacing)
	{
		diffuse = LIGHTS_CalcDiffuse(6, vf_FragPos, vf_Normal);
	}
	else
	{
		diffuse = vec3(0);// Light.ambient;
	}
	
	vec3 ambient = Light.ambient;
	float attenuation = LIGHT_CalcAttenuation(Light, vf_FragPos);

	// diffuse *= attenuation;

	finalLight = (diffuse * vf_Color.xyz) + ambient;


	texcolor *= vec4(finalLight, 1);

	gl_FragColor = texcolor;
}
