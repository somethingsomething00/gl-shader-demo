
typedef struct
{
	u32 *data;
	u32 width;
	u32 height;
	u32 bpp;
} image;


image TEX_Load(const char *path, bool flip)
{
	image Image = {0};
	int w, h, channels;

	stbi_set_flip_vertically_on_load(flip);

	void *data = stbi_load(path, &w, &h, &channels, 4);

	assert(data);

	Image.data = data;
	Image.width = w;
	Image.height = h;
	Image.bpp = 4;

	// Max out alpha channel for now
	for(int i = 0; i < w * h; i++)
	{
		Image.data[i] |= 0xFF000000;
	}

	return Image;
}

enum
{
	TEX_FILTER_NEAREST = (1 << 0),
	TEX_FILTER_LINEAR = (1 << 1),
	TEX_CLAMP = (1 << 2),
};


u32 TEX_AllocateTexture(u32 width, u32 height, void *data, u32 flags)
{
	u32 id;
	glGenTextures(1, &id);

	u32 filter = GL_NEAREST;
	u32 wrap = GL_REPEAT;

	glBindTexture(GL_TEXTURE_2D, id);

	if(flags & TEX_FILTER_LINEAR) filter = GL_LINEAR;
	if(flags & TEX_FILTER_NEAREST) filter = GL_NEAREST;
	if(flags & TEX_CLAMP) wrap = GL_CLAMP_TO_EDGE;

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, filter);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, filter);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, wrap);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, wrap);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, 0);
	return id;
}

u32 TEX_AllocateTextureFromImage(image *Image, bool FreeAfterAllocation, u32 flags)
{
	u32 w = Image->width;
	u32 h = Image->height;
	void *data = Image->data;

	return TEX_AllocateTexture(w, h, data, flags);
}

