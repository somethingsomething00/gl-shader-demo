#version 130

uniform sampler2D TexDither;
uniform sampler2D TexFrame;

in vec2 vf_TexCoord;
in vec4 vf_Color;

// Source for this implementation
// https://www.anisopteragames.com/how-to-fix-color-banding-with-dithering/

void main()
{
	vec4 texColor = texture(TexFrame, vf_TexCoord);

	vec2 sampleCoord = (gl_FragCoord.xy / 8.0);
	float ditherSample = texture(TexDither, sampleCoord).r;
	float noise = (ditherSample / 32.0) - (1.0 / 256.0);

	vec3 outcolor = (texColor.xyz * vf_Color.xyz) + noise;
	float alpha = texColor.a * vf_Color.a;

	vec4 output = vec4(outcolor, alpha);
	gl_FragColor = output;
}
